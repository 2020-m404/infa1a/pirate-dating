﻿using System;

namespace Pirate_Dating
{
    class Program
    {
        // Starting point of C# program
        // (the common language runtime CLR calls this "main method")
        static void Main(string[] args)
        {
            // Declare a variable of type pirate
            Pirate jack;

            // Assign a value to the declared value.
            // Use the new operator to create a new instance/object of a class.
            jack = new Pirate();

            // Access any member of an object using the member access operator (.)
            jack.name = "Jack";

            // Create an other variable (called emily) and initialize it with another pirate instance.
            Pirate emily = new Pirate();

            // Use the member access operator again to assign a string value ("Emily") as the new pirates value for its name field.
            emily.name = "Emily";

            // Assign the value of the second variable (emily) to the "inRelationWith" field of the first pirate (jack).
            jack.inLoveWith = emily;

            // Create an instance of class Gang...
            Gang piratesOfTheCaribbean = new Gang();
            piratesOfTheCaribbean.name = "Pirates of the Caribbean";

            // ... and assign at least one Pirate object to the gangs "member" field.
            /*
             * Following code won't work, since we haven't initialized the array "members":
             * 
             * gang.members[0] = jack;
             * gang.members[1] = emily;
             * 
             * Initialization of members field happens in the Gang class' constructor:
             * gang.members = new Pirate[50];
             */

            piratesOfTheCaribbean.members[0] = jack;
            piratesOfTheCaribbean.members[1] = emily;

            // Create another Gang instance
            Gang freibeuter = new Gang("Freibeuter");
            freibeuter.members[0] = emily;
            freibeuter.members[1] = jack;

            Console.WriteLine($"Name der Gang: {freibeuter.name}");

            // Create new ship instance
            Ship titanic = new Ship();

            // Use OBJECT INITIALIZER to set initial values for an object.
            // Using object initializers, one can assign values to public instance variables.
            // The parantheses of parameter-less constructors might be omitted.
            // Object initializers must be coupled to a constructor call.
            Pirate john = new Pirate { name = "John", Age = 21 };

            // Constructors are executed first.
            // Value assignments from object initializers happen after the constructor has finished.
            Pirate mary = new Pirate("Mary") { name = "mary" };
            Console.WriteLine($"Name of pirate: {mary.name}");  // mary

            Weapon sword = new Weapon();
            sword.Strength = 6.8f;                  // write accessor (setter)
            float strengthOfSword = sword.Strength; // read accessor (getter)

            // You cannot assign a valut to the Material property since it is read-only.
            // sword.Material = "Wood"; // invalid code!

            john.gang = piratesOfTheCaribbean;
            mary.gang = piratesOfTheCaribbean;
            piratesOfTheCaribbean.leader = mary;
            Console.WriteLine($"John is leader: {john.IsGangLeader}");  // false
            Console.WriteLine($"Mary is leader: {mary.IsGangLeader}");  // true

            float newTirednessOfJohn = john.Sleep(2.5); // before method call, tiredness was 5 (default value)
            john.Sleep(10);
            john.Sleep(24);

            Pirate myJohn = john;
        }
    }
}