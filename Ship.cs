﻿namespace Pirate_Dating
{
    class Ship
    {
        // Piraten, welche auf diesem Schiff segeln.
        public Pirate[] pirates;

        // The actual speed this ship is currently sailing.
        // Must be lower or equal than the ships max speed.
        public float actualSpeedInKnots;

        // The max speed this ship can sail.
        public float maxSpeedInKnots;

        // If a class has no (custom) constructor, the compiler generates the default constructor.
        // The default constructor has no parameters.
        public Ship()
        {
            // mache öppis...
        }

    }
}
