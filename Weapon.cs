﻿namespace Pirate_Dating
{
    class Weapon
    {
        // Type of weapon such as musket, pistol, sword, dagger
        public string type;

        // This is an auto-implemented property with GETTER and SETTER accessors.
        // Since it has getter and setter, it's also called a read-write property.
        public float Strength { get; set; }

        // This is an auto-implemented property with GETTER only.
        // It's also called a read-only property.
        // There are no auto-implemented "write-only" properies (setter only).
        // You can assign values to read-only properies either as default value or in a constructor.
        public string Material { get; } = "Iron";

        public Weapon()
        {
            // Assign a value to the read-only property in the constructor.
            Material = "Bronze";
        }


    }
}