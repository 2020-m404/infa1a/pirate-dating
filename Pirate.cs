﻿using System;

namespace Pirate_Dating
{
    // Declare class named Pirate. As per convention, class names start with a capital letter.
    // Within a namespace, names of classes must be unique.
    // Convention: The opening bracket follows on the line after the class name.
    class Pirate
    {
        // Between the opening and closing brackets are MEMBERS of the class declared.
        // This is the declaration of a field which consists of:
        //  - access modifider (public)
        //  - type (string)
        //  - identifier (name)
        public string name;

        // Backing field for property Age.
        // Here, the actual age of a pirate is stored.
        private int age;

        // Self-implemented read-write property with custom backing field.
        // Setter includes validation --> no pirate may be younger than 0 years old.
        public int Age
        {
            get => age; // short for: { return age; }
            set
            {
                if (value >= 0)
                {
                    // Store value of "value" keyword in backing field "age".
                    age = value;
                }

                // If given value is out of range (<0), the age is not changed.
            }
        }

        // To which gang belongs this pirate
        public Gang gang;

        // On which ship is this pirate sailing
        public Ship sailingOn;

        // What weapons does this pirate have
        public Weapon[] weapons;

        // With which (other) pirate is this pirate in relation (love)
        public Pirate inLoveWith;

        // This is a self-implemented read-only property.
        // The properties value is calculated based on other instance variables.
        public bool IsGangLeader
        {
            get
            {
                return gang != null && gang.leader == this;
            }
        }

        // Tiredness of pirate on a scale from 0 to 10 where 10 is maximal tired.
        private float tiredness = 5;

        // Let VS create an empty constructor: ctor [tab] [tab]
        public Pirate()
        {
            weapons = new Weapon[2];
        }

        public Pirate(string nameOfPirate)
        {
            name = nameOfPirate;
        }

        public float Sleep(double durationInHours)
        {
            const float minTiredness = 0;
            const float recommendedHoursOfSleep = 8.0f;
            const float biologicalSleepFactor = 1.362f;  // pure fictional value :-)
            if (durationInHours > 0)
            {
                double recreation = (durationInHours / recommendedHoursOfSleep) * biologicalSleepFactor;
                tiredness = (float)Math.Max(minTiredness, tiredness - recreation);
            }
            return tiredness;

        }
    }
}
